import React from 'react';

export const Store = {
    base_image: null,
    results: []
};

export const Reducer = (state, action) => {
    switch (action.type) {
        case 'set_base_image':
            return { ...state, base_image: action.value }
        case 'set_results':
            return { ...state, results: action.value }
        default:
            break;
    }
};

export const Context = React.createContext();