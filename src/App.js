import React, { useReducer, useRef } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { Context, Store, Reducer } from './context';
import Axios from 'axios'

function App() {
  const [store, dispatch] = useReducer(Reducer, Store);
  const inputFile = useRef(null)

  const onUpload = (e) => {
    if (e) {
      let blob = URL.createObjectURL(e);

      dispatch({ type: 'set_base_image', value: blob })
    }
  }

  const search = async () => {
    let form = new FormData()
    form.append('image', inputFile.current.files[0])

    await Axios.post('http://localhost:8000/image-retrieval', form)
      .then(res => dispatch({ type: 'set_results', value: res.data.result }))
  }

  return (
    <Context.Provider value={{ store, dispatch }}>
      <div>
        <button
          onClick={() => inputFile.current.click()}>Upload</button>
        <input ref={inputFile} hidden accept="image/*" type="file" onChange={e => onUpload(e.target.files[0])}/>
        <div>
          { store.base_image && <img src={store.base_image} className="m-2" height={64}/> }
        </div>
        <div>
          { store.base_image && <button onClick={() => search()}>Search</button> }
        </div>
        <div>
          { store.results.length > 0 && store.results.map((d, i) => <img className="m-2" src={`http://localhost:5000/${d}`} height={64}/>) }
        </div>
      </div>
    </Context.Provider>
  );
}

export default App;
